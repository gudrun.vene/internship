package com.internship.management.controller;

import com.internship.management.exception.CustomerNotFoundException;
import com.internship.management.exception.EmailTakenException;
import com.internship.management.model.Customer;
import com.internship.management.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
@NoArgsConstructor
@RequestMapping("/api")
public class CustomerController {


    @Autowired
    CustomerRepository customerRepository;

    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }


    @GetMapping("/customers/{id}")
    public Customer getCustomer(@PathVariable("id") Long customerId) {
        return customerRepository.findById(customerId)
                .orElseThrow(() ->
                        new CustomerNotFoundException("The customer could not be found."));
    }

    @PostMapping("/customers")
    public Customer createCustomer(@RequestBody @Valid Customer customer) {
        Optional<Customer> customerOptional= customerRepository
                .findCustomerByEmail(customer.getEmail());
        if(customerOptional.isPresent()){
            throw new EmailTakenException("Email already taken.");
        }
        return customerRepository.save(customer);
    }

    @PutMapping("/customers")
    public Customer updateCustomer(@RequestBody Customer customer) {
        return customerRepository.save(customer);
    }

    @DeleteMapping("/customers/{id}")
    public void deleteCustomer(@PathVariable("id") Long customerId) {
        customerRepository.deleteById(customerId);
    }


}
