package com.internship.management.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> errHandler(MethodArgumentNotValidException err) {
        Map<String, String> errors = new HashMap<>();
        err.getBindingResult()
                .getFieldErrors()
                .forEach(error -> errors.put(error.getField(), error.getDefaultMessage()));
        return errors;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CustomerNotFoundException.class)
    public Map<String, String> customerNotFound(CustomerNotFoundException err) {
        Map<String, String> error = new HashMap<>();
        error.put("error", err.getMessage());
        return error;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(OrderNotFoundException.class)
    public Map<String, String> orderNotFound(OrderNotFoundException err) {
        Map<String, String> error = new HashMap<>();
        error.put("error", err.getMessage());
        return error;
    }

    @ResponseStatus(HttpStatus.IM_USED)
    @ExceptionHandler(EmailTakenException.class)
    public Map<String, String> emailTaken(EmailTakenException err) {
        Map<String, String> error = new HashMap<>();
        error.put("error", err.getMessage());
        return error;
    }
}
