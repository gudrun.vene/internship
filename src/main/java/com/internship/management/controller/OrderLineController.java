package com.internship.management.controller;

import com.internship.management.model.OrderLine;
import com.internship.management.repository.OrderLineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OrderLineController {

    @Autowired
    OrderLineRepository orderLineRepository;

    @GetMapping("/orderlines")
    public List<OrderLine> getAllOrderLines() {
        return orderLineRepository.findAll();
    }
    @PostMapping("/orderlines")
    public OrderLine createOrderLine(@RequestBody OrderLine orderLine) {
        return orderLineRepository.save(orderLine);
    }
}
