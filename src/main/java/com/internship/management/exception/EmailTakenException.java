package com.internship.management.exception;

public class EmailTakenException extends RuntimeException{
    public EmailTakenException() {
        super();
    }

    public EmailTakenException(String message) {
        super(message);
    }
}
