

insert into customers (full_name, email, telephone)
values ('Darci Coventry', 'dcoventry0@prweb.com', 7682238732);
insert into customers (full_name, email, telephone)
values ('Dayna Ewbanks', 'dewbanks1@skyrock.com', 8869348711);
insert into customers (full_name, email, telephone)
values ('Susanna Fontin', 'sfontin2@wix.com', 6306317190);
insert into customers (full_name, email, telephone)
values ('Grannie Hasell', 'ghasell3@google.ru', 7064993805);
insert into customers (full_name, email, telephone)
values ('Cora Silverson', 'csilverson4@csmonitor.com', 3235394917);
insert into customers (full_name, email, telephone)
values ('Dalia Aslet', 'daslet5@goodreads.com', 5137407807);
insert into customers (full_name, email, telephone)
values ('Mattheus Jencken', 'mjencken6@soundcloud.com', 8632064702);
insert into customers (full_name, email, telephone)
values ('Westbrooke Pirdy', 'wpirdy7@surveymonkey.com', 4782510942);
insert into customers (full_name, email, telephone)
values ('Emili Bartley', 'ebartley8@whitehouse.gov', 6072246987);
insert into customers (full_name, email, telephone)
values ('Pamelina Bratt', 'pbratt9@ucsd.edu', 6904330925);



insert into orders (date_of_submission, customer_id)
values ('2022-05-21 19:31:08', 9);
insert into orders (date_of_submission, customer_id)
values ('2021-10-22 15:26:24', 6);
insert into orders (date_of_submission, customer_id)
values ('2022-01-26 19:13:10', 10);
insert into orders (date_of_submission, customer_id)
values ('2022-05-13 10:03:47', 2);
insert into orders (date_of_submission, customer_id)
values ('2022-02-08 05:36:28', 5);
insert into orders (date_of_submission, customer_id)
values ('2021-12-14 23:50:46', 1);
insert into orders (date_of_submission, customer_id)
values ('2022-06-17 07:47:21', 4);
insert into orders (date_of_submission, customer_id)
values ('2022-03-01 05:33:34', 9);
insert into orders (date_of_submission, customer_id)
values ('2021-11-25 10:20:31', 1);
insert into orders (date_of_submission, customer_id)
values ('2021-10-26 22:31:52', 2);

insert into products (name, unit_price)
values ('Soup - Campbells', 3.37);
insert into products (name, unit_price)
values ('Dried Peach', 2.21);
insert into products (name, unit_price)
values ('Red Pepper Paste', 3.48);
insert into products (name, unit_price)
values ('Soup - Knorr, Ministrone', 4.81);
insert into products (name, unit_price)
values ('Oil - Sesame', 4.43);
insert into products (name, unit_price)
values ('Wine - Fontanafredda Barolo', 3.86);
insert into products (name, unit_price)
values ('Soup - Campbellschix Stew', 2.75);
insert into products (name, unit_price)
values ('Pepper - Green Thai', 4.3);
insert into products (name, unit_price)
values ('Mcgillicuddy Vanilla Schnap', 4.99);
insert into products (name, unit_price)
values ('Cookie - Oreo 100x2', 1.01);

insert into orderlines (quantity, sku_code, order_id)
values (18, 1, 7);
insert into orderlines (quantity, sku_code, order_id)
values (21, 8, 3);
insert into orderlines (quantity, sku_code, order_id)
values (3, 10, 6);
insert into orderlines (quantity, sku_code, order_id)
values (21, 4, 3);
insert into orderlines (quantity, sku_code, order_id)
values (20, 7, 7);
insert into orderlines (quantity, sku_code, order_id)
values (15, 5, 7);
insert into orderlines (quantity, sku_code, order_id)
values (23, 5, 9);
insert into orderlines (quantity, sku_code, order_id)
values (15, 8, 10);
insert into orderlines (quantity, sku_code, order_id)
values (25, 3, 7);
insert into orderlines (quantity, sku_code, order_id)
values (23, 9, 9);
