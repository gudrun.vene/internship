package com.internship.management.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //TODO: generate 8 alphanumeric digits
    private long skuCode;

    private String name;

    private Double unitPrice;


}
