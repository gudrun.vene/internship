package com.internship.management.controller;

import com.internship.management.model.Customer;
import com.internship.management.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.powermock.api.mockito.PowerMockito;

import static org.junit.Assert.assertEquals;

import java.util.Optional;


class CustomerControllerTest {


    @Test
    public void testGetCustomer() {
        CustomerRepository mockDao = PowerMockito.mock(CustomerRepository.class);
        Customer mockCustomer = new Customer(2L, "Nick Fury", "fury@gmail.com", 555728954L);
        PowerMockito.when(mockDao.findById(2L)).thenReturn(Optional.of(mockCustomer));

        CustomerController customerController = new CustomerController(mockDao);
        Customer customer = customerController.getCustomer(2L);
        assertEquals(mockCustomer, customer);

    }
}