package com.internship.management.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "orders")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"dateOfSubmission"}, allowGetters = true)
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, updatable = false)
    // to test with data
//    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date dateOfSubmission;

    @ManyToOne
    private Customer customer;

    @OneToMany
    @JoinColumn(name = "order_id")
    private List<OrderLine> orderLines;


}
